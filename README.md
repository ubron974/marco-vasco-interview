# Question
## Mixed content
### How would you manage a “forget password” functionality. Describe the web forms and the database tables required to achieve that functionality (no code required, but a text description)

Voici un scénario possible :

Dans un premier temps, sur le formulaire de connexion, il existe un bouton "Réinitialiser mon mot de passe" qui nous redirige vers une page de réinitialisation.

Sur cette page, un formulaire contenant un champ "email" et un bouton "envoyer" nous est affiché

Lors de l'envoi de ce formulaire, notre application va chercher dans la table utilisateur si l'email envoyé correspond à un email connu. Si c'est le cas, dans une table appelé reinit_password, nous allons insérer une nouvelle ligne contenant l'id de l'utilisateur du mail, une date de création et un token auto généré. En parallèle, une nouvelle page est affichée à l'utilisateur l'indiquant qu'un mail lui sera envoyé.

Un mail est alors envoyé à notre utilisateur qui cherche à réinitialiser son mot de passe si et seulement si un compte lié à son mail existe. Ce mail contient un lien vers la page de modification du mot de passe. Ce lien contient comme paramètre GET le token auto généré afin de nous permettre d'identifier la personne dont nous avons modifier le mot de passe.

Une fois que l'utilisateur clique sur le lien, notre application va vérifier si le token existe bien. Si c'est le cas, elle va nous afficher une page contenant un formulaire de modification de mot de passe.

Ce formulaire est composé d'un champ mot de passe et vérifier le mot de passe ainsi qu'un champ caché token.

On envoie ensuite le formulaire au serveur qui va vérifier si les mots de passe correspondent. Si ce n'est pas le cas alors on reaffiche la page avec un message d'erreur. Sinon, on récupère l'id du user en fonction du token, on modifie le champ password_hash dans la table utilisateur et enfin, on supprime la ligne qui contient le token dans la table reinit_password

### What is for you the newest discovery on web technology (language/tool/workflow) you are the most excited about ? explain why in a few words

Vite js : un outil de build qui permet de délivrer le code front-end en utilisant les modules EcmaScript natif
De plus, il est beaucoup plus simple à manipuler que webpack.

### What is dependency injection concept and what problem it's trying to solve ?

L'injection de dépendance est un design pattern qui préconise d'injecter une instance d'un service dans une autre classe.
```php
interface Humain{} ou abstract class Humain{}
class Maison {
    private Humain $proprietaire;
    // Ceci est de l'injection de dependance
    public function __construct(Humain $personne)
    {
        $this->proprietaire = $personne;
    }
}
```
Cela permet de :
* séparer la création d'une dépendance de son usage.
* une maintenabilité de nos classes beaucoup plus simple.
* facilité les tests unitaires.

## PHP

### How would you sort an array on several columns?

On peut utiliser la fonction array_multisort.
Voici un petit exemple :
```php
$array = [
    [
        "prenom"=>"Bruno",
        "nom"=>"LAURET"    
    ],
    [
        "prenom"=>"Olena",
        "nom"=>"LAURET"    
    ],
    [
        "prenom"=>"Jean",
        "nom"=>"BON"    
    ]
];

$result = array_multisort(
	array_column($array, "nom"),
	SORT_DESC,
	array_column($array, "prenom"),
	SORT_DESC,
	$array
); 
```
Resultat
```
array(3) {
  [0]=>
  array(2) {
    ["prenom"]=>
    string(5) "Olena"
    ["nom"]=>
    string(6) "LAURET"
  }
  [1]=>
  array(2) {
    ["prenom"]=>
    string(5) "Bruno"
    ["nom"]=>
    string(6) "LAURET"
  }
  [2]=>
  array(2) {
    ["prenom"]=>
    string(4) "Jean"
    ["nom"]=>
    string(3) "BON"
  }
} 
```

### What is an SQL injection? How do you avoid it?

Une injection SQL est une faille de sécurité qui permet d'injecter du code SQL malveillant dans une requête SQL en cours d'exécution.
En PHP pour éviter les injections SQL, on peut utiliser :
* La fonction mysqli_real_escape_string avant d'exécuter la requête SQL.
* Les requêtes préparées de PDO. C'est d'ailleurs la meilleure méthode en PHP natif.
* Utiliser un ORM du marché qui gère les injections SQL

## Coding
Pour executer le script :
* faire un composer install
* exceuter la comande **php play**

## MySQL

### What the following query should return (describe) ?

Dans cette requête, nous allons récupérer des "guides" (```leads.assigned_user_id = users.id```) qui doivent respecter les règles suivantes :
* Ce sont des utilisateurs actifs et non supprimés du système
* Le responsable de chaque guide est impérativement un utilisateur actif
* L'équipe de chaque guide doit impérativement commencer par "EQ "
* Les tours/leads affectés à un guide doivent avoir une date de départ inférieur à la date d'aujourd'hui + 200 jours.
**Ex :** Si la date d'aujourd'hui est le 01/01/2023 alors la date de départ doit etre inférieur au 20/07/2023.
* Le nombre de tours/leads effectué avec les conditions precedentes doit être plus grand ou egale au nombre maximum de tours/leads que peut effectuer un guide. Nous pouvons récupérer cette information grâce au GROUP BY et HAVING suivant :
```sql
GROUP BY users.id
HAVING count( DISTINCT leads.id ) >= users.maximal_leads
```

Les informations de la requête à afficher sont :
* Le nom de l'équipe du guide
* Id utilisateur du guide
* Son nom d'utilisateur
* Son prénom
* Son nom
* Le nombre de tours/leads maximal qu'il peut faire
* Le nombre de tours/leads effectué dans les conditions defini en amont

Il faut noter que le résultat sera trié par ordre croissant en fonction du nom de l'équipe dans un premier temps et du nom de l'utilisateur dans un deuxième temps. 

### How would you return several lines in the same column by SQL Query (ex : one user is linked to several destination, return the username + the list of destination in the same line)

J'utiliserai un `GROUP BY` et la fonction `GROUP_CONCAT`.
Voici un exemple de requete
```sql
SELECT u.username, GROUP_CONCAT(d.city) as destination
FROM user u
INNER JOIN destination d ON u.id=d.id_user
GROUP BY u.id
```

## Jquery / Ajax / Json

### How would you check all the checkboxes where the id starts with “user_check_box” ?
Voici le code demandé
```js
$("[id^=user_check_box][type=checkbox]").each(function() {
  $(this).prop( "checked", true);
});
```

### What would be for you, the best skeleton to create a jquery plugin ?
Voici un skeleton de plugin Jquery :
```js
(function($){
  $.fn.yourcustomfunction = function(){
    // Code associé à la fonction
  }
})(Jquery)
```
### What is requirejs or commonjs ? Did you use one ? What makes this kind of tool so useful ?
**What is requirejs or commonjs ?**

Requirejs et commonjs permettent d'écrire du JavaScript de manière modulaire.
C'est-à-dire grosierrement qu'il est possible d'importer du code js dans un autre code js (un peu comme require ou include en php).
Voici un exemple :
```js
// hello.js

module.exports = class Hello {
  static sayHello() {
    console.log("Hello World!!!");
  }
}
```
```js
// index.js

const Hello = require("./hello.js");
Hello.sayHello();
```

**Did you use one ?**

Oui, dans le cadre de projets en node js.

**What makes this kind of tool so useful ?**

Cela permet de produire du code js bien organiser et beaucoup plus maintenable.

## Docker

### What’s the difference between a Dockerfile and a Docker compose file ?

Le dockerfile est un fichier qui permet de décrire une "image" docker.
En effet, ce fichier contient toutes les instructions qui nous permettent de construire l'image.

**Exemple :** 
Dans ce cas, on construit cette image en 2 étapes
* **Etape 1 :** on compile du Typescript pour générer du js(tsc). Puis on compile le js afin de générer un exécutable (pkg)
* **Etape 2 :** on copie l'exécutable dans l'image et lors du lancement de container, on l'execute
```DOCKERFILE
FROM node:14.16.1-alpine3.13 as build
COPY ./ /app
WORKDIR /app
RUN npm install --unsafe-perm \
    && ./node_modules/.bin/tsc \
    && ./node_modules/.bin/pkg --target node14-alpine-x64 --output bot dist/app.js

FROM alpine as final
COPY --from=build /app/bot ./app/bot
COPY --from=build /app/strat.json ./strat.json
RUN adduser -S appuser \
    && chown -R appuser /app \
    && apk update \
    && apk add --no-cache libstdc++ libgcc \
    && rm -rf /var/cache/apk/*
USER appuser
ENTRYPOINT [ "/app/bot" ]
```

Le fichier docker-compose permet de définir tous les services qui vont permettre de lancer notre application.
Elle nous permet aussi de gérer d'autres paramètres liés à Docker comme par exemple les volumes et le réseau.
**Exemple :**
Dans cet exemple, on va lancer plusieurs services :
* une base mongodb
* un bot qui va venir stocker de la donnée dans la base mongo
* l'outil Metabase et sa base Postgres associée pour faire de la visualisation de donné qui sera exposé sur le web via Traefik(défini dans un autre dockercompose)
```yaml
version: "3.9"
services:
  datastore:
    image: mongo:latest
    restart: unless-stopped
    networks:
      - app
    volumes:
      - mongo-data:/data/db
    environment:
      MONGO_INITDB_ROOT_USERNAME: ${MONGO_USERNAME}
      MONGO_INITDB_ROOT_PASSWORD: ${MONGO_PASSWORD}
      MONGO_INITDB_DATABASE: ${MONGO_DB}
  dataviewerdb:
    image: postgres:13.2-alpine
    restart: unless-stopped
    networks:
      - app
    environment:
      POSTGRES_USER: ${POSTGRES_USER}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
      POSTGRES_DB: ${POSTGRES_DB}
      PGDATA: /data/postgres
    volumes:
        - postgres-data:/data/postgres
  dataviewer:
    image: metabase/metabase:v0.39.3
    restart: unless-stopped
    networks:
      - app
      - front
    depends_on: 
      - dataviewerdb
      - datastore
    expose: 
      - "3000"
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=front"
      - "traefik.http.routers.metabase.rule=Host(`${METABASE_DOMAIN}`)"
      - "traefik.http.routers.metabase.entrypoints=web"
      - "traefik.http.services.servmetabase.loadbalancer.server.port=3000"
      - "traefik.http.routers.metabase.service=servmetabase"
      #- "traefik.http.routers.metabase.tls=true"
      #- "traefik.http.routers.metabase.tls.certresolver=letsencrypt"
    environment:
      JAVA_TIMEZONE: Europe/Paris
      MB_DB_TYPE: postgres
      MB_DB_DBNAME: ${POSTGRES_DB}
      MB_DB_PORT: 5432
      MB_DB_USER: ${POSTGRES_USER}
      MB_DB_PASS: ${POSTGRES_PASSWORD}
      MB_DB_HOST: dataviewerdb
  bot_quatre:
    image: registry.gitlab.com/otyrcal-poc/bot:0.4.3
    depends_on: 
      - datastore
    networks:
      - app
    environment: 
      - MONGO_USERNAME=${MONGO_USERNAME}
      - MONGO_PASSWORD=${MONGO_PASSWORD}
      - MONGO_RO_USERNAME=${MONGO_RO_USERNAME}
      - MONGO_RO_PASSWORD=${MONGO_RO_PASSWORD}
      - MONGO_DB=${MONGO_DB}
      - MONGO_DOMAIN=datastore
      - MONGO_PORT=27017
volumes:
  mongo-data:
    name: "mongo-data"
  metabase-data:
    name: "metabase-data"
  postgres-data:
    name: "postgres-data"
networks:
  front:
    external: true
  app:
    name: app
    driver: bridge
```