<?php
namespace Game;

class App
{    
    public function run() : void
    {
        $game = new Game(
            new Player("jean", 120, 90),
            new Player("jacques", 50, 120),
            new GameRender()
        );
        $game->simulatedPlay();
    }
}
