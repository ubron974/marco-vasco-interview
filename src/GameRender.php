<?php
namespace Game;

class GameRender
{
    public function renderPlayerTurn(Player $p) : void
    {
        echo "\e[34mTour de ".$p->getName().PHP_EOL;
    }

    public function renderPunchInfo(Player $p, int $powerPunch) : void
    {
        echo "\e[92m".$p->getName()." envoie une attaque de puisance \e[1m $powerPunch".PHP_EOL;
    }

    public function renderDamageInfo(Player $p, int $damage) : void
    {
        echo "\e[91m".$p->getName()." est endommagé à hauteur de \e[1m $damage".PHP_EOL;
        echo "\e[91mIl lui reste \e[1m".$p->getLife()." \e[25mde vie".PHP_EOL;
    }

    public function renderWinner(Player $p)
    {
        echo "\e[92m".$p->getName()." a gagné".PHP_EOL;
    }

    public function renderPlayer(Player $p)
    {
        # code...
    }
}
