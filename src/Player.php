<?php
namespace Game;

class Player
{
    private int $life = 100;

    public function __construct(
        private string $name,
        private int $offenceLevel,
        private int $defenceLevel
    ) {}

    public function isKo() : bool
    {
        return $this->life<=0;
    }

    public function getLife() : int
    {
        return $this->life<0?0:$this->life;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getOffenceLevel() : string
    {
        return $this->offenceLevel;
    }

    public function getDefenceLevel() : string
    {
        return $this->defenceLevel;
    }

    public function receiveDamage(int $punchPower) : int
    {
        $damage = $punchPower-$this->defenceLevel;
        $damage = $damage<0?0:$damage;
        $this->life -= $damage;
        return $damage; 
    }

    public function getPunchPower() : int
    {
        return $this->offenceLevel+random_int(0, 100);
    }
}
