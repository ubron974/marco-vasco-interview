<?php
namespace Game;

class Game
{
    private readonly array $players;
    private GameRender $gr;
    public function __construct(
        Player $playerOne,
        Player $playerTwo,
        GameRender $gr
    ){
        $this->players = [
            $playerOne,
            $playerTwo
        ];
        $this->gr = $gr;
    }

    public function punch(Player $current, Player $opponant) : void
    {
        $powerPunch = $current->getPunchPower();
        $this->gr->renderPunchInfo($current, $powerPunch);
        $damage = $opponant->receiveDamage($powerPunch);
        $this->gr->renderDamageInfo($opponant, $damage);
    }
    
    public function simulatedPlay() : void
    {
        $turnId = 0;
        do {
            $currentPlayer = $this->players[$turnId++%2];
            $opponantPlayer = $this->players[$turnId%2];
            $this->gr->renderPlayerTurn($currentPlayer);
            $this->punch($currentPlayer, $opponantPlayer);
        } while (!$currentPlayer->isKo() && !$opponantPlayer->isKo());
        $this->gr->renderWinner($currentPlayer);
    }

}
